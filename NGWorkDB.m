//
//  NGWorkDB.m
//  test
//
//  Created by Naz on 9/24/15.
//  Copyright © 2015 Naz. All rights reserved.
//

#import "NGWorkDB.h"
#import "UIImageView+AFNetworking.h"

static NSString *dataStoreName = @"test.xcdatamodeld";

@interface NGWorkDB()

@property (nonatomic, strong) NSPersistentStore * dataStore;
@property (nonatomic, strong) NSManagedObjectModel *model;
@property (nonatomic, strong) NSPersistentStoreCoordinator *coordinator;

@end

@implementation NGWorkDB

@synthesize dataStore;

+(NGWorkDB *)date{
    
    static NGWorkDB* dataBaseCon;
    if(!dataBaseCon){
        dataBaseCon = [[NGWorkDB alloc] init];
    }
    return dataBaseCon;
}

-(instancetype)init{
    self = [super init];
    if (!self) {return nil;}
    
    self.model = [NSManagedObjectModel mergedModelFromBundles:nil];
    self.coordinator = [[NSPersistentStoreCoordinator alloc]
                        initWithManagedObjectModel:self.model];
    self.managedObjectContext = [[NSManagedObjectContext alloc]
                                 initWithConcurrencyType:NSMainQueueConcurrencyType];
    [self.managedObjectContext setPersistentStoreCoordinator:self.coordinator];
    return self;
}
-(NSPersistentStore *)dataStore{
    if (!dataStore){
        NSError *error = nil;
        dataStore = [self.coordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                   configuration:nil
                                                             URL:[self storeURL]
                                                         options:nil error:&error];
    }
    
    return dataStore;
}
- (NSString *)applicationDocumentsDirectory {
    
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES) lastObject];
}
- (NSURL *)applicationStoresDirectory {
    NSURL *storesDirectory =
    [[NSURL fileURLWithPath:[self applicationDocumentsDirectory]]
     URLByAppendingPathComponent:@"Stores"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:[storesDirectory path]]) {
        NSError *error = nil;
        if ([fileManager createDirectoryAtURL:storesDirectory
                  withIntermediateDirectories:YES
                                   attributes:nil
                                        error:&error]) {
        }
        else {
            NSLog(@"FAILED to create Stores directory: %@", error);
        }
    }
    return storesDirectory;
}

- (NSURL *)storeURL {
    
    return [[self applicationStoresDirectory]
            URLByAppendingPathComponent:dataStoreName];
    
}
-(void)setupCoreData {
    
    dataStore = [self.coordinator addPersistentStoreWithType:NSSQLiteStoreType
                                               configuration:nil
                                                         URL:[self storeURL]
                                                     options:nil error:nil];
}

-(void)save
{
    NSError *error = nil;
    
    if ([self.managedObjectContext save:&error]) {
        NSLog(@"...");
    } else {
        NSLog(@"Failed to save _context: %@", error);
    }
}

-(void)addNewProductWithName:(NSString *) name
                    addCount:(NSInteger) count
                    addImage:(NSString *) image
                    addPrice:(NSString *) price
                     addType:(NSString *) type {
    
    Product *prod = [NSEntityDescription
                     insertNewObjectForEntityForName:@"Product"
                     inManagedObjectContext:self.managedObjectContext];;
    prod.name = name;
    prod.count = [NSNumber numberWithInteger:count];
    prod.image = image;
    prod.price = price;
    prod.type = type;

     [self.managedObjectContext insertObject:prod];
     [self  save];
    
}


-(void)removeProductWithObject:(Product *)product{
    
    [self.managedObjectContext deleteObject:product];
    [self  save];
}

-(NSArray *)getAllProduct{
    
    NSManagedObjectContext *moc = [self managedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription
                                              entityForName:@"Product" inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    
    // Set example predicate and sort orderings...
    [request setPredicate:nil];
    
    NSError *error;
    NSArray *array = [moc executeFetchRequest:request error:&error];
    return array;
}

-(Product *)getProductWithName:(NSString *)name {
    
    NSManagedObjectContext *moc = [self managedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription
                                              entityForName:@"Product" inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    
    
    NSPredicate *preficate = [NSPredicate predicateWithFormat:@"Product like %@", name];
    [request setPredicate:preficate];
    // Set example predicate and sort orderings...
    
    NSError *error;
    NSArray *array = [moc executeFetchRequest:request error:&error];
    return [array firstObject];
}

- (void) deleteAllObjects: (NSString *) entityDescription {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:_managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *items = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
   
    
    
    for (NSManagedObject *managedObject in items) {
        [_managedObjectContext deleteObject:managedObject];
        NSLog(@"%@ object deleted",entityDescription);
    }
    if (![_managedObjectContext save:&error]) {
        NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
    }

}
@end
