//
//  Product+CoreDataProperties.m
//  
//
//  Created by Naz on 9/24/15.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Product+CoreDataProperties.h"

@implementation Product (CoreDataProperties)

@dynamic count;
@dynamic image;
@dynamic name;
@dynamic price;
@dynamic type;
//@dynamic imageData;

@end
