//
//  User+CoreDataProperties.m
//  
//
//  Created by Naz on 9/25/15.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "User+CoreDataProperties.h"

@implementation User (CoreDataProperties)

@dynamic idUser;
@dynamic tokenVk;

@end
