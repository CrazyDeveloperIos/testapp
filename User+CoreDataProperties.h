//
//  User+CoreDataProperties.h
//  
//
//  Created by Naz on 9/25/15.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "User.h"

NS_ASSUME_NONNULL_BEGIN

@interface User (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *idUser;
@property (nullable, nonatomic, retain) NSString *tokenVk;

@end

NS_ASSUME_NONNULL_END
