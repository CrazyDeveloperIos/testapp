//
//  AddProductViewController.h
//  test
//
//  Created by Naz on 9/21/15.
//  Copyright © 2015 Naz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddProductViewController : UITableViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>


@end

