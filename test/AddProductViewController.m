//
//  AddProductViewController.m
//  test
//
//  Created by Naz on 9/21/15.
//  Copyright © 2015 Naz. All rights reserved.
//

#import "AddProductViewController.h"
#import "NGServerManager.h"
#import "AFNetworking.h"
#import "SCLAlertView.h"


@interface AddProductViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageProduct;
@property (weak, nonatomic) IBOutlet UITextField *countTextField;
@property (weak, nonatomic) IBOutlet UITextField *typeTextField;
@property (weak, nonatomic) IBOutlet UITextField *chategoryTextField;
@property (weak, nonatomic) IBOutlet UITextField *priceTextField;
@property (strong, nonatomic) NSString* filePach;
@property (weak, nonatomic) IBOutlet UIButton *postButton;

@end

@implementation AddProductViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.postButton.enabled = NO;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW,(ino64_t)(5.0 * NSEC_PER_SEC)) , dispatch_get_main_queue(), ^{
        self.postButton.enabled = YES;
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
}

- (IBAction)selectPhoto:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
}

- (IBAction)postNewProduct:(id)sender {
    
    if (self.filePach != (NSString*)[NSNull null]) {
        
    if (self.countTextField.text.length != 0 && self.typeTextField.text.length != 0 && self.chategoryTextField.text.length != 0 && self.priceTextField.text.length != 0) {  //&& [self.filePach isEqualToString:@""]
    
    [self.view endEditing:YES];
        
     NSString *count = [NSString stringWithFormat:@"%@%@%@", @"#",self.countTextField.text, @" "];
     NSString *type = [NSString stringWithFormat:@"%@%@%@", @"#",self.typeTextField.text, @" "];
     NSString *chategory = [NSString stringWithFormat:@"%@%@%@", @"#",self.chategoryTextField.text, @" "];
     NSString *price = [NSString stringWithFormat:@"%@%@%@", @"#",self.priceTextField.text, @" "];
     NSString* finishString = [NSString stringWithFormat:@"%@%@%@%@", count, type, chategory, price];
    
     NSLog(@"Pressed Button");
    [[NGServerManager sharedManager]
    addAlbom:finishString
 addFilePath:self.filePach
   onSuccess:^(NSDictionary *dict) {
         
         NSLog(@"Success");
         
         
         
         SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
         
         [alert showSuccess:@"Perfect:)" subTitle:@"Post Product Done" closeButtonTitle:@"" duration:3.0f];
     }
     
     
     onFailure:^(NSError *error) {
         NSLog(@"");
     }];
    } else {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showError:self title:@"Oh!" subTitle:@"Input all cell!" closeButtonTitle:@"OK" duration:0.0f]; // Error

    }
    } else {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showError:self title:@"Oh!" subTitle:@"Select image!" closeButtonTitle:@"OK" duration:0.0f]; // Error

    }

}

#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.imageProduct.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [paths firstObject];
    if (path) {
        self.filePach = [path stringByAppendingPathComponent:@"Image.png"];
        
        // Save image.
        [UIImagePNGRepresentation(chosenImage) writeToFile:self.filePach atomically:YES];
        
    }
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (IBAction)takePhoto:(UIButton *)sender {
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])  {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];

    }
    else {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showError:self title:@"Oh!" subTitle:@"This Devise Has Not Camera!" closeButtonTitle:@"OK" duration:0.0f]; // Error
    }
    
}
@end
