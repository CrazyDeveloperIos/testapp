//
//  DetailTableViewController.m
//  test
//
//  Created by Naz on 9/24/15.
//  Copyright © 2015 Naz. All rights reserved.
//

#import "DetailTableViewController.h"

@interface DetailTableViewController ()
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *cathegoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIButton *getbutton;

@end

@implementation DetailTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.getbutton.enabled = NO;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW,(ino64_t)(3.0 * NSEC_PER_SEC)) , dispatch_get_main_queue(), ^{
        self.getbutton.enabled = YES;
    });
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}
-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}


- (IBAction)getProduct:(id)sender {
    NSLog(@"Pressed button");
}

@end
