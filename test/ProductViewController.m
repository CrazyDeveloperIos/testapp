//
//  ProductViewController.m
//  test
//
//  Created by Naz on 9/25/15.
//  Copyright © 2015 Naz. All rights reserved.
//

#import "ProductViewController.h"
#import "AFNetworking.h"
#import "SCLAlertView.h"
#import "UIImageView+AFNetworking.h"
#import "NGProductTableViewCell.h"
#import "NGServerManager.h"
#import "NGWallGroup.h"
#import "NGWorkDB.h"
#import "Product.h"

@interface ProductViewController ()<VKSdkDelegate>

@property (strong, nonatomic) NSString *textResponce;
@property (strong, nonatomic) NSString *userID;
@property (nonatomic ,strong) UIRefreshControl *refreshView;
@property (strong, nonatomic) NSString *id_user;
@property (strong, nonatomic) NSString* tokenVk;
@property (nonatomic, strong) NSMutableArray *postArray;
@property (nonatomic, strong) NSMutableArray *productArray;

@end

@implementation ProductViewController

- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    self.productArray = [NSMutableArray new];
    self.postArray = [NSMutableArray array];
    [VKSdk initializeWithDelegate:self andAppId:@"5053942"];
    if ([VKSdk wakeUpSession])
    {
        
    }
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.linksTableView.delegate = self;
    self.linksTableView.dataSource = self;
    [self getProducts];
    self.refreshView = [[UIRefreshControl alloc] init];
    
    [self.refreshView addTarget:self action:@selector(getProducts) forControlEvents:UIControlEventValueChanged];
    
    [self.tableView addSubview:self.refreshView];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString* token = [defaults objectForKey:@"token"];
    
    if ([token isEqualToString:@""]) {
        
            NSArray *scope = @[@"friends",@"photos",@"audio",@"wall"];
            [VKSdk authorize:scope revokeAccess:YES forceOAuth:YES];
    }
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController setToolbarHidden:YES];
     self.productArray = [[[NGWorkDB date] getAllProduct] mutableCopy];
}

-(void)vkSdkShouldPresentViewController:(UIViewController *)controller{
    
    [self.navigationController presentViewController:controller animated:YES completion:nil];
}

-(void)vkSdkNeedCaptchaEnter:(VKError *)captchaError{
    
    
}
-(void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken{
    
    
}
-(void)vkSdkUserDeniedAccess:(VKError *)authorizationError{
    
    
}
-(void)vkSdkReceivedNewToken:(VKAccessToken *)newToken{
    
    self.tokenVk = newToken.accessToken;
    self.userID = newToken.userId;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:newToken.accessToken forKey:@"token"];
    [defaults synchronize];
    
    [self getProducts];
    [self.tableView reloadData];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}
-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) getProducts{
    
    [[NGServerManager sharedManager]
     addowner_id:@"-102872314"
     addalbum_id:@"0"
     addaudio_ids:@"0"
     addneed_user:@"10"
     addoffset:@"all"
     addcount:@""
     addversion:@"5.37"
     
     onSuccess:^(NSArray *dict) {
         
         [self.postArray removeAllObjects];
         [self.postArray addObjectsFromArray:dict];
         
         NSMutableArray* newPaths = [NSMutableArray array];
         for (int i = (int)[self.postArray count] - (int)[dict count]; i < [self.postArray count]; i++) {
             [newPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
         }
          self.productArray = [[[NGWorkDB date] getAllProduct] mutableCopy];
         [self.refreshView endRefreshing];
         [self.tableView reloadData];
         
         
     }
     onFailure:^(NSError *error) {
         NSLog(@"");
     }];
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.productArray.count;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 600;
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    return YES;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    if (self.tableView == tableView){
        static NSString *simpleTableIdentifier = @"Cell";
        
        NGProductTableViewCell *cell = [self.linksTableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if(!cell){
            cell = [[NGProductTableViewCell alloc] initWithStyle:
                    UITableViewCellStyleDefault      reuseIdentifier:simpleTableIdentifier];
        }
        
        Product *taxi = [self.productArray objectAtIndex:indexPath.row];
        
        //cell.detailTextLabel.text = [NSString stringWithFormat:@"%d", [taxi.phoneNamber intValue]];
        NSString* photo = [NSString stringWithFormat:@"%@", taxi.image];
        NSURL *imageURL;
        if (photo) {
            imageURL = [NSURL URLWithString:photo];
        }
        [cell.imageProfile setImageWithURL: imageURL];
        //[cell.imageProfile setImageWithURL:imageURL placeholderImage:nil];
        
        //cell.imageProfile.image = [UIImage imageWithData:taxi.imageData];
        cell.priceLabel.text = (NSString*)taxi.price;
        cell.nicknameLabel.text = taxi.name;
        
        
        return cell;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

@end
