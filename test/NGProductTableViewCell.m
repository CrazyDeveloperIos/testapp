//
//  NGProductTableViewCell.m
//  test
//
//  Created by Naz on 9/25/15.
//  Copyright © 2015 Naz. All rights reserved.
//

#import "NGProductTableViewCell.h"

@implementation NGProductTableViewCell

- (void)awakeFromNib {
    
    self.imageProfile.layer.cornerRadius = 15;
    self.imageProfile.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
