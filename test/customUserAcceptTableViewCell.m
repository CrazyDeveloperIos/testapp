//
//  customUserAcceptTableViewCell.m
//  test
//
//  Created by Naz on 9/21/15.
//  Copyright © 2015 Naz. All rights reserved.
//

#import "customUserAcceptTableViewCell.h"

@implementation customUserAcceptTableViewCell

- (void)awakeFromNib {
        self.imageProfile.layer.cornerRadius = 15;
        self.imageProfile.layer.masksToBounds = YES;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
