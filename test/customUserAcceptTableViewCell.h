//
//  customUserAcceptTableViewCell.h
//  test
//
//  Created by Naz on 9/21/15.
//  Copyright © 2015 Naz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface customUserAcceptTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nicknameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageProfile;

@end
