//
//  NGWorkDB.h
//  test
//
//  Created by Naz on 9/24/15.
//  Copyright © 2015 Naz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Product.h"
@interface NGWorkDB : NSObject
-(NSArray *) getAllProduct;
-(Product *) getProductWithName:(NSString *)name;
-(void)save;
-(void)setupCoreData;
-(void)addNewProductWithName:(NSString *)name
                    addCount:(NSInteger)count
                    addImage:(NSString *) image
                    addPrice:(NSString *)price
                     addType:(NSString *) type;
-(void) removeProductWithObject:(Product *) product;
-(void) deleteAllObjects: (NSString *) entityDescription;
+(NGWorkDB *) date;
@property (nonatomic) NSManagedObjectContext *managedObjectContext;
@end

